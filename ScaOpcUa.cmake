#
# ScaOpcUa.cmake
#
# Created on: 21 Mar 2021
# Author: Paris Moschovakos (paris.moschovakos@cern.ch)
# 
#      CMake file for the SCA server configuration
#      

message(STATUS "Using file [ScaOpcUa.cmake] toolchain file")

# -----------------------
# SCA Software specifics
# -----------------------

option(HAVE_NETIO "Whether to support netio backend" ON)
option(HAVE_NETIO_NEXT "Whether to support netio-next backend" ON)
option(LOGIT_BACKEND_UATRACE "Whether to build with UATRACE (logging via UASDK logger)" ON)
set(Boost_NO_WARN_NEW_VERSIONS 1)

SET( SCASW_LIBS
    $ENV{FELIX_ROOT}/lib/libfelix-client-thread.so
    $ENV{FELIX_ROOT}/lib/libfelix-client-lib.so
    $ENV{FELIX_ROOT}/lib/libnetio.so
    $ENV{FELIX_ROOT}/lib/libhdlc_coder.so
    $ENV{FELIX_ROOT}/lib/libtbb.so.2
    $ENV{PROTOBUF_DIRECTORIES}/libprotobuf.so
)

SET( SCASW_LIBDIRS
    $ENV{FELIX_ROOT}/lib
    $ENV{PROTOBUF_DIRECTORIES}
)

SET( SCASW_HEADERS
    $ENV{PROTOBUF_HEADERS}
)

message(STATUS "Taking SCA Software dependencies from environment:")
message(STATUS "SCA Software headers [${SCASW_HEADERS}]")
message(STATUS "SCA Software lib directories [${SCASW_LIBDIRS}]")
message(STATUS "SCA Software libs [${SCASW_LIBS}]")

# -----------------------
# General CMake settings
# -----------------------
set(CMAKE_CXX_COMPILER $ENV{CXX})
set(CMAKE_CC_COMPILER $ENV{CC})

if(NOT DEFINED CMAKE_CXX_COMPILER_LAUNCHER)
  find_program(CCACHE_PROGRAM ccache)
  if (CCACHE_PROGRAM)
    set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}" CACHE STRING "Compiler launcher for CXX")
  endif()
endif()

# ------------------------------------------
# OPC UA - Settings for OpcUaToolkit 1.6.5
# ------------------------------------------
if(DEFINED ENV{OPCUA_TOOLKIT_PATH})
  set(OPCUA_TOOLKIT_PATH $ENV{OPCUA_TOOLKIT_PATH})
  message(
    STATUS
      "Taking OPC UA Toolkit path from the environment: ${OPCUA_TOOLKIT_PATH}")
else(DEFINED ENV{OPCUA_TOOLKIT_PATH})
  set(OPCUA_TOOLKIT_PATH "/winccoa/rpms/toolkits/uasdk-1.6.5-gcc11.1.0-with_OPCUA-2561")
  message(
    STATUS
      "Taking OPC UA Toolkit path from the CMake config file: ${OPCUA_TOOLKIT_PATH}"
  )
endif(DEFINED ENV{OPCUA_TOOLKIT_PATH})

set(OPCUA_TOOLKIT_LIBS_DEBUG
    "-luamoduled -lcoremoduled -luabasecppd -luastackd -luapkicppd -lxmlparsercppd -lxml2 -lssl -lcrypto -lpthread -lrt"
)
set(OPCUA_TOOLKIT_LIBS_RELEASE
    "-luamodule -lcoremodule -luabasecpp -luastack -luapkicpp -lxmlparsercpp -lxml2 -lssl -lcrypto -lpthread -lrt"
)

include_directories(
  ${OPCUA_TOOLKIT_PATH}/include/uastack ${OPCUA_TOOLKIT_PATH}/include/uabasecpp
  ${OPCUA_TOOLKIT_PATH}/include/uaservercpp ${OPCUA_TOOLKIT_PATH}/include/uapkicpp
  ${OPCUA_TOOLKIT_PATH}/include/xmlparsercpp)

add_custom_target(quasar_opcua_backend_is_ready)

# ------------------------- 
# Other quasar dependancies 
# -------------------------
# This dependancy is probably for quasar rather than the server cmake configuration file
set(XML_LIBS -lxerces-c)

add_definitions(-Wall -DBACKEND_UATOOLKIT -std=gnu++17 -Wno-deprecated -Wno-literal-suffix -Wno-unused-local-typedefs)
