%define name OpcUaScaServer
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /opt/%{name}
%undefine _missing_build_ids_terminate_build

AutoReqProv: yes 
Summary: %{name}
Name: %{name}
Version: %{version}
Release: %{release}
Source0: checkout.tar.gz
License: zlib/libpng license
Group: Development/Application
BuildRoot: %{_topdir}/BUILDROOT/%{name}-%{version}
BuildArch: x86_64
Prefix: %{PREFIX}
Vendor: CERN 

%description
OPC-UA for GBT-SCA.
https://gitlab.cern.ch/atlas-dcs-opcua-servers/ScaOpcUa

%prep
echo ">>> setup tag" 
echo %{name}

%setup -n checkout 

%build
echo "--- Build ---"
source /opt/felix/felix.sh
unset PYTHONPATH
source ScaSoftware/setup_paths.sh || exit 1
python3 quasar.py set_build_config ScaOpcUa.cmake || exit 1
python3 quasar.py build Release || exit 1
python3 quasar.py generate as_doc || exit 1
python3 quasar.py generate config_doc || exit 1
python3 quasar.py generate diagram 3 || exit 1

%install
source /opt/felix/felix.sh || exit 1
source ScaSoftware/setup_paths.sh || exit 1
echo "--- Install (don't confuse with installation; nothing is installed on your system now in fact...) ---"
INSTALLED_DIR=%{buildroot}/%{PREFIX}/bin
INSTALLED_LIB_DIR=%{buildroot}/%{PREFIX}/lib
INSTALLED_DOC_DIR=%{buildroot}/%{PREFIX}/doc
INSTALLED_DIR_CONFIG_EXAMPLES=%{buildroot}/%{PREFIX}/extra/config_examples
/bin/mkdir -p $INSTALLED_DIR
/bin/mkdir -p $INSTALLED_LIB_DIR
/bin/mkdir -p $INSTALLED_DOC_DIR
/bin/mkdir -p $INSTALLED_DIR_CONFIG_EXAMPLES
/bin/cp -v \
    build/bin/OpcUaScaServer \
    build/Configuration/Configuration.xsd \
    bin/ServerConfig.xml \
    $INSTALLED_DIR # <-- destination

/bin/cp -vL \
    $PROTOBUF_DIRECTORIES/libprotobuf.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_atomic*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_date_time*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_chrono*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_filesystem*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_program_options*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_regex*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_system*.so.* \
    $BOOST_LIB_DIRECTORIES/libboost_thread*.so.* \
    "`dirname $CXX`/../lib64/libstdc++.so.6" \
    $INSTALLED_LIB_DIR

/bin/cp -v \
        Documentation/AddressSpaceDoc.html \
	Documentation/ConfigDocumentation.html \
	Design/diagram.pdf \
	$INSTALLED_DOC_DIR  # <-- destination

/bin/cp -v bin/config-sim.xml $INSTALLED_DIR_CONFIG_EXAMPLES/config-sample-sca-simulator.xml

# the following line fixes the XSD path of Configuration.xsd to system-wide in /opt so any config file could be opened no matter where it is ...
sed -e "s/\.\.\/Configuration/\/opt\/OpcUaScaServer\/bin/g" -i $INSTALLED_DIR_CONFIG_EXAMPLES/config-sample-sca-simulator.xml

# here you give a list of other files to install

%pre
echo "Pre-install: nothing to do"

%post
echo "Generating OPC UA Server Certificate..."
%{PREFIX}/bin/OpcUaScaServer --create_certificate


%preun

if [ $1 = 0 ]; then
	echo "Pre-uninstall: Complete uninstall: will remove files"
fi

%postun
if [ $1 = 0 ]; then
    echo "Post-uninstall: Complete uninstall: will remove files"
fi

# 
# Hint: if your server installs any shared objects, you should run ldconfig here. 
#

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{PREFIX}
# additional paths ...

%changelog
