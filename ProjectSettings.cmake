set(CUSTOM_SERVER_MODULES ScaSoftware )
set(EXECUTABLE OpcUaScaServer)
set(SERVER_INCLUDE_DIRECTORIES ${SCASW_HEADERS} )
set(SERVER_LINK_LIBRARIES ${SCASW_LIBS}   # SCASW_LIBS normally is defined in configuration file, might be empty string
  )
set(SERVER_LINK_DIRECTORIES ${SCASW_LIBDIRS} )