# For running the SCA server with felix-star applications please prepare the environment accordingly
# setting the relevant felix client properties.
#
# This is an example. Please refer to felix team for further information about the required properties.
# 

export FELIX_CLIENT_bus_dir="/afs/cern.ch/work/n/nswdaq/public/nswdaq/current/felix/bus"
export FELIX_CLIENT_bus_group_name="FELIX"
export FELIX_CLIENT_log_level="error"
export FELIX_CLIENT_netio_pages="128"
export FELIX_CLIENT_netio_pagesize="3"
export FELIX_CLIENT_timeout="1000"
export FELIX_CLIENT_local_ip_or_interface="enp2s0f1"
export FELIX_CLIENT_verbose_bus="False"