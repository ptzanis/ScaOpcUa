/*
 * ScaSupervisor.cpp
 *
 *  Created on: 8 Aug 2018
 *      Author: pnikiel
 */

#include <unistd.h>  // for sleep, TODO: maybe change too boost?

#include <LogIt.h>

#include <ScaSupervisor.h>
#include <Sca/Request.h>

#include <Utils.h>

ScaSupervisor::ScaSupervisor()
:m_continueBackgroundTask(false)
{
    LOG(Log::TRC, s_logComponent) << "ScaSupervisor: instantiating!";
    m_numberOffline = 0;
    s_logComponent = Log::getComponentHandle("Superv");
    if (s_logComponent == Log::INVALID_HANDLE)
        THROW_WITH_ORIGIN(std::logic_error, "order of initialization wrong or component name wrong?");
}

ScaSupervisor::~ScaSupervisor()
{
    this->stopBackgroundTask(); // if not done already ...
}

void ScaSupervisor::stopBackgroundTask()
{
    if (m_continueBackgroundTask)
    {
        LOG(Log::INF, s_logComponent) << "ScaSupervisor: stopping background task";
        m_continueBackgroundTask = false;
        m_backgroundThread.join();
        LOG(Log::INF, s_logComponent) << "ScaSupervisor: background task stopped";
    }
}

ScaSupervisor* ScaSupervisor::instance()
{
    std::lock_guard<decltype(s_instanceControlLock)> lock (s_instanceControlLock);
    if (!s_instance)
        s_instance = new ScaSupervisor();

    return s_instance;
}

void ScaSupervisor::kill()
{
    std::lock_guard<decltype(s_instanceControlLock)> lock (s_instanceControlLock);
    if (s_instance)
    {
        delete s_instance;
        s_instance = nullptr;
    }
}

GhostPointer<Sca::Sca> ScaSupervisor::requireSca(const RequireRequest& request)
{
    LOG(Log::TRC, s_logComponent) << "requireSca( address=" << request.address << " ) ";
    std::lock_guard<decltype(this->m_accessLock)> lock (this->m_accessLock);
    GhostPointer<Sca::Sca> targetReference;
    bool success = attemptInitialize(request, targetReference, false);
    if (success)
        return m_scas.back().reference;
    else
    {
        LOG(Log::INF, s_logComponent) << "Deferring initialization of SCA: " << request.address;
        DeferredScaHolder holder;
        holder.request = request;
        holder.reference = targetReference;
        m_deferredScas.push_back(holder);
        return holder.reference;
    }
}

std::mutex ScaSupervisor::s_instanceControlLock;
ScaSupervisor* ScaSupervisor::s_instance = nullptr;

void ScaSupervisor::backgroundTask()
{
    while(m_continueBackgroundTask)
    {
        sleep(1);
        {
            std::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
            processInitialized();
            processDeferred();
            processExternalRequests();
        }
    }
}

/** If there are any deferred SCAs left, prints the info. */
void ScaSupervisor::printSummary()
{
    if (!m_deferredScas.empty())
    {
        LOG(Log::INF, s_logComponent) << "ScaSupervisor: #initialized SCAs=" << m_scas.size() << ", #deferred SCAs=" << m_deferredScas.size();
    }
}

/**
 * Tries to initialize given SCA: construct the object and run the initializer.
 * If any of that fails, returns false.
 * IMPORTANT: This is an internal function therefore not synchronized!
 */
bool ScaSupervisor::attemptInitialize(
        const RequireRequest& request,
        GhostPointer<Sca::Sca>& target,
        bool                    runInitializerDelegate)
{
    LOG(Log::TRC, s_logComponent) << "attemptInitialize( address=" << request.address << " ) ";
    try
    {
        m_scas.emplace_back( request );
    }
    catch (const std::exception &e)
    {
        LOG(Log::ERR, s_logComponent) << "SCA unreachable. Check the connectivity of the SCA at address " << request.address;
        LOG(Log::DBG, s_logComponent) << "It was impossible to construct the SCA object: " << e.what();
        return false;
    }
    // If we're here, the SCA ctr has been successfully finished and it is the last element on the m_scas list
    if (request.checkId)
    {
        unsigned int id = m_scas.back().sca.getChipId();
        if (id != request.requestedId)
        {
            LOG(Log::ERR, s_logComponent) << "ID constraint failed for SCA:" << request.address << " requested=" << request.requestedId << " got=" << id;
            m_scas.pop_back();
            return false;
        }
    }
    m_scas.back().reference = target;
    m_scas.back().reference.assign (&m_scas.back().sca);
    m_scas.back().reference.enableAccess();

    if (runInitializerDelegate)
    {
        try
        {
            request.initializeFunction( *m_scas.back().reference);
        }
        catch (const std::exception &e)
        {
            LOG(Log::ERR, s_logComponent) << "Initialization failed for SCA: " << request.address << ": " << e.what();
            m_scas.pop_back();
            return false;
        }
    }
    return true;
}

unsigned int ScaSupervisor::getNumberOffline()
{
    return m_numberOffline + m_deferredScas.size();
}

void ScaSupervisor::processDeferred()
{
    decltype(m_deferredScas)::iterator it = m_deferredScas.begin();
    while(it != m_deferredScas.end())
    {
        DeferredScaHolder& holder = *it;
        LOG(Log::TRC, s_logComponent) << "Processing deferred SCA " << holder.request.address;
        if (attemptInitialize(holder.request, holder.reference))
        {
            LOG(Log::INF, s_logComponent) << "Deferred initialization was successful for: " << holder.request.address;
            holder.reference = m_scas.back().reference;
            holder.reference.enableAccess();
            it = m_deferredScas.erase(it);
            if (m_deferredScas.empty())
            {
                LOG(Log::INF, s_logComponent) << "Deferred initialization completed.";
            }
        }
        else
            ++it;
    }
    printSummary();
}

void ScaSupervisor::processInitialized()
{
    // ping SCAs which should be online
     unsigned int numberOffline = 0;
     for (InitializedScaHolder& holder : m_scas)
     {

         if (!holder.lastPongState)
         {
             // before we send anything, HDLC reconnect is a good idea since we think the connection was lost
             LOG(Log::WRN, s_logComponent) << "Warning: sending HDLC reconnect to " << holder.request.address << " before next ping.";
             holder.sca.reconnect();
         }
         LOG(Log::TRC, s_logComponent) << "Pinging SCA";
         auto t1 = std::chrono::high_resolution_clock::now();
         bool pong = holder.sca.ping();
         auto t2 = std::chrono::high_resolution_clock::now();
         double rttUs = std::chrono::duration_cast< std::chrono::microseconds> ( t2 - t1 ).count();
         LOG(Log::TRC, s_logComponent) << "Ping result for SCA " << holder.request.address << ": " << pong << " (it took " << rttUs << " us)";

         if (holder.request.notification)
             holder.request.notification( pong, rttUs );

         try
         {
             if (holder.lastPongState != pong)
             {
                 // TODO here keep the last state to print out when the SCA stops ponging
                 if (pong)
                 {
                     // The SCA is pinging again - what sort of reinitialization should we do?
                     // Check if the SCA has (a) channel enabled, if not it means it was completely down
                     Sca::Sca* sca = holder.reference.get();
                     bool adcEnabled = sca->getChannelEnabled(Sca::Constants::ADC);
                     if (adcEnabled)
                     {
                         // if ADC is enabled then most likely it was just a communication failure
                         // and the SCA has not been reset
                         act( holder.request.recoveryActionScaStayedPowered, holder );
                     }
                     else
                     {
                         // if ADC is not enabled then most likely the SCA was reset
                         act( holder.request.recoveryActionScaWasRepowered, holder );
                     }

                     holder.reference.enableAccess();
                 }
                 else
                     holder.reference.disableAccess();
                 holder.lastPongState = pong;
             }
         } catch (...) {} // if we fail this time, will redo the same in the next iteration.
         if (!pong)
             numberOffline ++;

     }
     m_numberOffline = numberOffline;
}

ResetFromAddressSpace ScaSupervisor::parseResetFromAddressSpace(const std::string& resetFromAddressSpace)
{
    if (resetFromAddressSpace == "denied")
        return ResetFromAddressSpace::DENIED;
    else if (resetFromAddressSpace == "unconditional")
        return ResetFromAddressSpace::UNCONDITIONAL;
    else if (resetFromAddressSpace == "only_if_kaputt")
        return ResetFromAddressSpace::ONLY_IF_KAPUTT;
    else
        throw_runtime_error_with_origin("Value out of enumeration: " + resetFromAddressSpace);

}

void ScaSupervisor::act(
        const RecoveryAction& action,
        InitializedScaHolder& holder)
{
    LOG(Log::INF, s_logComponent) << "Act on SCA " << holder.request.address << " " << recoveryActionToString(action);
    switch( action )
    {
    case DO_NOTHING: break;
    case CONFIGURE:
    {
        holder.request.initializeFunction( *holder.reference.get() );
        break;
    }
    case RESET_AND_CONFIGURE:
    {
        holder.sca.reset(); // this also enables all channels
        holder.request.initializeFunction( *holder.reference.get() );
        break;
    }
    }
}

RecoveryAction ScaSupervisor::parseRecoveryAction(const std::string& action)
{
    if (action == "do_nothing")
        return RecoveryAction::DO_NOTHING;
    else if (action == "only_configure")
        return RecoveryAction::CONFIGURE;
    else if (action == "reset_and_configure")
        return RecoveryAction::RESET_AND_CONFIGURE;
    else
        throw_runtime_error_with_origin("Value out of enumeration: "+action);
}

std::string ScaSupervisor::recoveryActionToString(const RecoveryAction& action)
{
    switch (action)
    {
    case DO_NOTHING: return "do_nothing"; break;
    case CONFIGURE: return "only_configure"; break;
    case RESET_AND_CONFIGURE: return "reset_and_configure"; break;
    default: return "XXX fixme pnikiel";
    }

}

/** This normally will be called from another thread like OPC-UA address-space.
 * Note this implementation is not so good... we should improve it as we go.
 * TODO: Find a better way of passing this request between the SCA Supervisor and the address-space.
 */
bool ScaSupervisor::requireReset(const std::string& address, bool onlyIfKaputt)
{
    {
        std::lock_guard<decltype(m_externalResetRequestsLock)> lock (m_externalResetRequestsLock);
        m_externalResetRequests.emplace_back( address, onlyIfKaputt );
    }
    while (true)
    {
        {
            std::lock_guard<decltype(m_externalResetRequestsLock)> lock (m_externalResetRequestsLock);
            decltype(m_externalResetRequests)::iterator it = std::find_if(
                    m_externalResetRequests.begin(),
                    m_externalResetRequests.end(),
                    [address](const ExternalResetRequest& r){return r.address==address;});
            if (it == m_externalResetRequests.end())
                throw std::logic_error("Request miracoulously vanished?");
            if (it->finished)
            {
                bool finishedSuccessfully = it->finishedSuccessfully;
                m_externalResetRequests.erase(it);
                return finishedSuccessfully;
            }
        }
        usleep(100000);
    }
}

void ScaSupervisor::processExternalRequests()
{
    std::lock_guard<decltype(m_externalResetRequestsLock)> lock (m_externalResetRequestsLock);
    for (ExternalResetRequest& r : m_externalResetRequests)
    {
        if (r.finished)
            continue;
        // find the initialized SCA to which this request refers to
        decltype(m_scas)::iterator it = std::find_if(m_scas.begin(), m_scas.end(), [r](const InitializedScaHolder& h){return h.request.address == r.address;});
        if (it == m_scas.end())
        {
            // this must be a wrong request as it doesn't refer to a particular initialized SCA
            LOG(Log::ERR, s_logComponent) << "The ExternalResetRequest refers to an unknown SCA, it must be logic error" << r.address;
            r.finished = true;
        }
        else
        {
            LOG(Log::INF, s_logComponent) << "Handling reset request to SCA " << r.address << " coming from OPC-UA address-space";
            r.finishedSuccessfully = true;
            try
            {
                if (r.resetOnlyIfKaputt)
                {
                    if (it->lastPongState != true)
                    {
                        act(RecoveryAction::RESET_AND_CONFIGURE, *it);
                    }
                    else
                    {
                        LOG(Log::WRN, s_logComponent) << "Reset request from the address space not executed because the mode is 'only_if_kaputt' and the SCA seems healthy.";
                        r.finishedSuccessfully = false;
                    }
                }
                else
                    act(RecoveryAction::RESET_AND_CONFIGURE, *it);
            }
            catch (...)
            {
                r.finishedSuccessfully = false; // due to an exception ...
            }
            r.finished = true;
        }

    }
}

//! This should be called from QuasarServer::initialize which is after the full server configuration gets loaded,
void ScaSupervisor::onServerInitialize()
{
    LOG(Log::INF, s_logComponent) << "ScaSupervisor::onServerInitialize, creating SCA Supervisor background thread.";

    for (InitializedScaHolder& holder : m_scas)
    {
        holder.request.initializeFunction(*holder.reference);
    }

    m_continueBackgroundTask = true;
    m_backgroundThread = std::thread(&ScaSupervisor::backgroundTask, this);

}

Log::LogComponentHandle ScaSupervisor::s_logComponent = Log::INVALID_HANDLE;
