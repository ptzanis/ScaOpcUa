/*
 * ScaStatistics.h
 *
 *  Created on: 19 Mar 2019
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_SCASTATISTICS_H_
#define DEVICE_INCLUDE_SCASTATISTICS_H_

#include <stdint.h>

struct ScaStatistics
{
    double   requestRate;
    uint64_t numberLostReplies;
    ScaStatistics() : requestRate(0), numberLostReplies(0) {}
};


#endif /* DEVICE_INCLUDE_SCASTATISTICS_H_ */
