
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt)
    on 2019-11-13T14:53:40.398+01:00

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.



 */





#ifndef __DDacOutput__H__
#define __DDacOutput__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>

#include <Configuration.hxx>

#include <Base_DDacOutput.h>

#include <GhostPointer.h>
namespace Sca { class Sca; }

#include <Sca/Defs.h>

namespace Device
{




class
    DDacOutput
    : public Base_DDacOutput
{

public:
    /* sample constructor */
    explicit DDacOutput (
        const Configuration::DacOutput & config,
        Parent_DDacOutput * parent
    ) ;
    /* sample dtr */
    ~DDacOutput ();




    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readVoltage (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeVoltage (
        OpcUa_Double &value
    );


    /* delegators for methods */


private:
    /* Delete copy constructor and assignment operator */
    DDacOutput( const DDacOutput & );
    DDacOutput& operator=(const DDacOutput &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    GhostPointer<Sca::Sca> sca() const { return m_sca; }

private:
    GhostPointer<Sca::Sca> m_sca;
    Sca::Constants::Dac m_id;


};





}

#endif // include guard
