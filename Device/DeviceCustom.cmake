set(DEVICE_CUSTOM_SOURCES 
	# here place the list of your custom sources from Device module,
	# e.g. src/MyFile1.cpp src/DeviceClass.cpp
	src/ScaSupervisor.cpp
    src/AdcSampler.cpp
    src/BitBangProtocol.pb.cc
	)