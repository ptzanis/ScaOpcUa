echo "make_distro: trying to create a package for commit id $1"
DIR=OpcUaScaServer-$1
rm -Rf $DIR $DIR".tar.gz"

mkdir -p $DIR/bin
mkdir -p $DIR/lib
mkdir -p $DIR/Configuration
mkdir -p $DIR/Documentation
mkdir -p $DIR/bin/ScaSoftware

cp -v build/bin/OpcUaScaServer \
    bin/ServerConfig.xml \
    bin/config-sim.xml \
    felixClientProperties_example.sh \
    $DIR/bin || exit 1

cp -v build/Configuration/Configuration.xsd  \
    $DIR/Configuration || exit 1

cp -v Documentation/ConfigDocumentation.html \
    Documentation/AddressSpaceDoc.html \
    $DIR/Documentation || exit 1

cp -vL ${FELIX_ROOT}/lib/libfelix-client-thread.so \
    ${FELIX_ROOT}/lib/libnetio.so \
    ${FELIX_ROOT}/lib/libhdlc_coder.so \
    ${FELIX_ROOT}/lib/libtbb.so.2 \
    ${FELIX_ROOT}/lib/libfabric.so.1 \
    ${FELIX_ROOT}/lib/libatomic.so.1 \
    ${FELIX_ROOT}/lib/libfelix-client-lib.so \
    ${FELIX_ROOT}/lib/libnetio-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-server-lib.so \
    ${FELIX_ROOT}/lib/libjwrite.so \
    ${FELIX_ROOT}/lib/libsimdjson.so \
    $DIR/lib || exit 1

cp -vL $PROTOBUF_DIRECTORIES/libprotobuf.so.* \
    $BOOST_ROOT/lib/libboost_atomic*.so.* \
    $BOOST_ROOT/lib/libboost_date_time*.so.* \
    $BOOST_ROOT/lib/libboost_chrono*.so.* \
    $BOOST_ROOT/lib/libboost_filesystem*.so.* \
    $BOOST_ROOT/lib/libboost_program_options*.so.* \
    $BOOST_ROOT/lib/libboost_regex*.so.* \
    $BOOST_ROOT/lib/libboost_system*.so.* \
    $BOOST_ROOT/lib/libboost_thread*.so.* \
    "`dirname $CXX`/../lib64/libstdc++.so.6" \
    "`dirname $CXX`/../lib64/libgcc_s.so.1" \
    $DIR/lib/ || exit 1

cp -vL /usr/lib64/libxerces-c-3.1.so $DIR/lib/ || exit 1

cp -vL ScaSoftware/build/Demonstrators/StandaloneI2cSimulation/standalone_i2c \
    ScaSoftware/build/Demonstrators/GbtxConfiguration/gbtx_configuration \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fscajtag \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fscajfile \
    ScaSoftware/build/Demonstrators/HenksFScaJtag/fxvcserver \
    ScaSoftware/build/Demonstrators/Timing/timing \
    ScaSoftware/Demonstrators/README.md $DIR/bin/ScaSoftware

tar cf $DIR".tar" $DIR
gzip -9 $DIR".tar"
